//Implementatiopn of cascade SVM, based on the paper: "Parallel
// Support Vector Machines: The Cascade SVM" by Hans Peter Graf,
// Eric Cosatto, Leon Bottou, Igor Durdanovic and Vladimir Vapnik
// (2005)
// Author: Kveldulfur Thastarson, kth68@hi.is, 2018

#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <mpi.h>
#include <H5Cpp.h>
#include "svm-comm.h"
#include <string.h>
#include <chrono>
#include <vector>

//for ease-of-use
#define Malloc(type, n) (type *)malloc((n) * sizeof(type))
//This one inits everything to 0
#define Calloc(type, n) (type *)calloc(n, sizeof(type))

enum file_type
{
    hdf5,
    svmlite
};

//constants
const H5std_string CLASSSET_NAME("class");           //double
const H5std_string FEATURESET_NAME("feature_value"); //float
const H5std_string DENSITYMAP_NAME("densitymap");    //bool

//
// svm_model
//
struct svm_model
{
    svm_parameter param; // parameter
    int nr_class;        // number of classes, = 2 in regression/one class svm
    int l;               // total #SV
    Xfloat **SV;
    int **nz_sv;
    int *sv_len;
    int max_idx;
    double **sv_coef; // coefficients for SVs in decision functions (sv_coef[n-1][l])
    double *rho;      // constants in decision functions (rho[n*(n-1)/2])
    double *probA;    // pariwise probability information
    double *probB;

    // for classification only

    int *label; // label of each class (label[n])
    int *nSV;   // number of SVs for each class (nSV[n])
    // nSV[0] + nSV[1] + ... + nSV[n-1] = l
    // XXX
    int free_sv; // 1 if svm_model is created by svm_load_model
    // 0 if svm_model is created by svm_train
};

//global variables
struct svm_model *model;
struct svm_model *model2;

struct svm_problem prob;
struct svm_problem initial_prob;
struct svm_problem prob_A;
struct svm_problem prob_B;
int *nz_idx_space;
Xfloat *x_space;

int cross_validation;
int dense_features;
int nr_fold;
int seg_count;
file_type input_file_type, output_file_type;

//dispose an svm_problem struct, leaving behind pointers contained in prob.x[*] and prob.nz_idx[*]
//this is safe to use on an svm_problem that has been used or made by concat_svm_problems()
void destroy_svm_problem_safe(svm_problem *prob)
{
    free(prob->y);
    free(prob->x);
    free(prob->nz_idx);
    free(prob->x_len);
    prob->y = NULL;
    prob->x = NULL;
    prob->nz_idx = NULL;
    prob->x_len = NULL;
    // free(prob);
}

void deep_copy_svm_problem(svm_problem *source, svm_problem *dest)
{
    dest->l = source->l;
    dest->max_idx = source->max_idx;
    dest->y =  Malloc(double, source->l);
    dest->x_len =  Malloc(int, source->l);
    dest->x =  Malloc(Xfloat*, source->l);
    dest->nz_idx =  Malloc(int*, source->l);
    for(int i = 0; i != source->l; i++)
    {
        dest->y[i] = source->y[i];
        dest->x_len[i] = source->x_len[i];
        dest->x[i] =  Malloc(Xfloat, source->x_len[i]);
        dest->nz_idx[i] =  Malloc(int, source->x_len[i]);
        for(int n = 0; n != source->x_len[i]; n++)
        {
            dest->x[i][n] = source->x[i][n];
            dest->nz_idx[i][n] = source->nz_idx[i][n];
        }
    }
}

//dispose an svm_model struct, leaving behind pointers contained in model.SV[*] and model.nz_sv[*]
//this is safe to use on an svm_model that has been used in convert_svm_model_to_problem()
void destroy_svm_model_safe(svm_model *model)
{
    for (int i = 0; i < model->nr_class - 1; i++)
    {
        free(model->sv_coef[i]);
    }
    free(model->SV);
    free(model->nz_sv);
    free(model->sv_len);
    free(model->sv_coef);
    free(model->rho);
    free(model->label);
    free(model->probA);
    free(model->probB);
    free(model->nSV);
}

//this function sends the provided pointerless_svm_problem to rank target
void send_svm_problem(int target, svm_problem *prob, MPI_Comm communicator)
{
    //send l first, as it is used later by the receiver
    MPI_Send(&prob->l, 1, MPI_INT, target, 1, communicator);
    //send y
    MPI_Send(prob->y, prob->l, MPI_DOUBLE, target, 1, communicator);
    //send x_len,a s we need it for the following sends
    MPI_Send(prob->x_len, prob->l, MPI_INT, target, 1, communicator);
    //nz_idx[n] and x[n] have same length, which is x_len[n]
    //send x
    for (int i = 0; i != prob->l; i++)
    {
        MPI_Send(prob->x[i], prob->x_len[i], MPI_FLOAT, target, 1, communicator);
    }
    //send nz_idx
    for (int i = 0; i != prob->l; i++)
    {
        MPI_Send(prob->nz_idx[i], prob->x_len[i], MPI_INT, target, 1, communicator);
    }
    //send max_idx
    MPI_Send(&prob->max_idx, 1, MPI_INT, target, 1, communicator);
}

//This function reveices a pointerless_svm_problem via MPI, and places into buffer.
//note buff should be an empty svm_problem, as it is allocateded here.
void recv_svm_problem(int sender, svm_problem *buff, MPI_Comm communicator)
{
    MPI_Status status;
    //receive in the same order as send_svm_problem sends, starting with l
    MPI_Recv(&buff->l, 1, MPI_INT, sender, 1, communicator, &status);
    //allocate and receive y
    buff->y = Malloc(double, buff->l);
    MPI_Recv(buff->y, buff->l, MPI_DOUBLE, sender, 1, communicator, &status);
    //allocate and receive x_len
    buff->x_len = Malloc(int, buff->l);
    MPI_Recv(buff->x_len, buff->l, MPI_INT, sender, 1, communicator, &status);
    //allocate and receive x
    buff->x = Malloc(float *, buff->l);
    for (int i = 0; i != buff->l; i++)
    {
        buff->x[i] = Malloc(float, buff->x_len[i]);
        MPI_Recv(buff->x[i], buff->x_len[i], MPI_FLOAT, sender, 1, communicator, &status);
    }
    //allocate and receive nz_idx
    buff->nz_idx = Malloc(int *, buff->l);
    for (int i = 0; i != buff->l; i++)
    {
        buff->nz_idx[i] = Malloc(int, buff->x_len[i]);
        MPI_Recv(buff->nz_idx[i], buff->x_len[i], MPI_INT, sender, 1, communicator, &status);
    }
    MPI_Recv(&buff->max_idx, 1, MPI_INT, sender, 1, communicator, &status);
}

//This function performs the broadcast operations needed to broadcast an entire
//populated svm_problem struct
void bcast_svm_problem(int root, int myrank, svm_problem *buff, MPI_Comm communicator)
{
    MPI_Bcast(&buff->l, 1, MPI_INT, root, communicator);

    //allocate and receive y
    if (myrank != root)
    {
        buff->y = Malloc(double, buff->l);
    }
    MPI_Bcast(buff->y, buff->l, MPI_DOUBLE, root, communicator);
    //allocate and receive x_len
    if (myrank != root)
    {
        buff->x_len = Malloc(int, buff->l);
    }
    MPI_Bcast(buff->x_len, buff->l, MPI_INT, root, communicator);
    //allocate and receive x
    if (myrank != root)
    {
        buff->x = Malloc(float *, buff->l);
    }
    for (int i = 0; i != buff->l; i++)
    {
        if (myrank != root)
        {
            buff->x[i] = Malloc(float, buff->x_len[i]);
        }
        MPI_Bcast(buff->x[i], buff->x_len[i], MPI_FLOAT, root, communicator);
    }
    //allocate and receive nz_idx
    if (myrank != root)
    {
        buff->nz_idx = Malloc(int *, buff->l);
    }
    for (int i = 0; i != buff->l; i++)
    {
        if (myrank != root)
        {
            buff->nz_idx[i] = Malloc(int, buff->x_len[i]);
        }
        MPI_Bcast(buff->nz_idx[i], buff->x_len[i], MPI_INT, root, communicator);
    }
    MPI_Bcast(&buff->max_idx, 1, MPI_INT, root, communicator);
}

//merge 2 svm_problems into a single, larger problem, suitable for use in cascade svm.
//note buff should be an empty svm_problem, as it is allocateded here.
//note as well that it is not safe to dispose of either probA, probB or buff, while
// any of the others are still in use, due to the shared pointers used for x[*] and nz_idx[*]
void concat_svm_problems(svm_problem *probA, svm_problem *probB, svm_problem *buff)
{
    //l is simple the combined lengths
    buff->l = probA->l + probB->l;

    //max_idx is the greater of the 2 values
    if (probA->max_idx > probB->max_idx)
    {
        buff->max_idx = probA->max_idx;
    }
    else
    {
        buff->max_idx = probB->max_idx;
    }

    //y needs to appear in buff, with the same order as probA.y + probB.y
    buff->y = Malloc(double, buff->l);
    for (int i = 0; i != probA->l; i++)
    {
        buff->y[i] = probA->y[i];
    }
    for (int i = 0; i != probB->l; i++)
    {
        buff->y[i + probA->l] = probB->y[i];
    }

    //x_len needs to appear in buff, with the same order as probA.x_len + probB.x_len
    buff->x_len = Malloc(int, buff->l);
    for (int i = 0; i != probA->l; i++)
    {
        buff->x_len[i] = probA->x_len[i];
    }

    for (int i = 0; i != probB->l; i++)
    {
        buff->x_len[i + probA->l] = probB->x_len[i];
    }

    //x needs to appear in buff, with the same order as probA.x + probB.x
    //we assign buff.x[0..buff.l] the pointer values 
    //from probA.x[0..probA.l-1] + probB.x[probA.l,buff.l]
    buff->x = Malloc(float *, buff->l);
    for (int i = 0; i != probA->l; i++)
    {
        buff->x[i] = probA->x[i];
    }
    for (int i = 0; i != probB->l; i++)
    {
        buff->x[i + probA->l] = probB->x[i];
    }

    //nz_idx needs to appear in buff, with the same order as probA.nz_idx + probB.nz_idx
    //we assign buff.nz_idx[0..buff.l] the pointer values from 
    //probA.nz_idx[0..probA.l-1] + probB.xnz_idx[probA.l,buff.l]
    buff->nz_idx = Malloc(int *, buff->l);
    for (int i = 0; i != probA->l; i++)
    {
        buff->nz_idx[i] = probA->nz_idx[i];
    }
    for (int i = 0; i != probB->l; i++)
    {
        buff->nz_idx[i + probA->l] = probB->nz_idx[i];
    }
}

//This function takes as input an svm_model, and converts it into an svm_problem
// containing the support vectors from svm_model
//note buff should be an empty svm_problem, as it is allocateded here.
//note: do not dispose of input_model fully after using this function, as the pointers
// in SV and nz_sv are reused in buff. use destroy_svm_model_safe() to dispose of it
void convert_svm_model_to_problem(svm_model *input_model, svm_problem *buff)
{
    //First we get the length fo the input model and max_idx
    buff->l = input_model->l;
    buff->max_idx = input_model->max_idx;
    //Alocate and assign y values, note: the model contains these values in
    //a strange way, where the values: nSV[0] + nSV[1] + .. + nSV[x-1] to
    // nSV[0] + nSV[1] + .. + nSV[x] have the label label[x]
    buff->y = Malloc(double, buff->l);
    int counter = 0;
    for (int i = 0; i != input_model->nr_class; i++)
    {
        for (int n = 0; n != input_model->nSV[i]; n++)
        {
            buff->y[counter + n] = static_cast<double>(input_model->label[i]);
        }
        //increment counter to track real position for y
        counter += input_model->nSV[i];
    }
    
    //The rest are comparatively simple: allocate, assign, repeat.
    buff->x_len = Malloc(int, buff->l);
    for (int i = 0; i != buff->l; i++)
    {
        buff->x_len[i] = input_model->sv_len[i];
    }
    buff->x = Malloc(float *, buff->l);
    for (int i = 0; i != buff->l; i++)
    {
        buff->x[i] = input_model->SV[i];
    }
    buff->nz_idx = Malloc(int *, buff->l);
    for (int i = 0; i != buff->l; i++)
    {
        buff->nz_idx[i] = input_model->nz_sv[i];
    }
}

void setup_range(int *range_low, int *range_up, int total_sz, int size)
{
    //evenly distribute with max 1 difference.
    //TODO: make this more elegant, its not a big issue as this is run once per core per run,
    //and even ginormous(technical term) line counts will be processed in insignificant time.
    int *lengths;
    lengths = Malloc(int, size);
    int counter = 0;
    for (int i = 0; i != total_sz; i++)
    {
        lengths[counter]++;
        counter++;
        counter %= size;
    }

    int tmp_low = 0;
    int tmp_high = lengths[0];
    for (int i = 0; i != size; i++)
    {
        range_low[i] = tmp_low;
        range_up[i] = tmp_high;
        tmp_low += lengths[i];
        tmp_high += lengths[i];
    }
}

void setup_range_hdf5(int *range_low, int *range_up, int total_linecount, int no_of_processors, int chunks_in_file)
{
    if (total_linecount <= chunks_in_file) //there is not one line per chunk, so we don't worry about the chunks and just divide the lines evenly between processors
    {
        setup_range(range_low, range_up, total_linecount, no_of_processors);
    }
    else
    {
        //Store each ranks lower and upper line count in range_low and range_up
        int lines_per_chunk = ((int)(total_linecount/chunks_in_file))+1; //Calculate how many lines are in each chunk based on total_linecount and chunks_in_file
        int chunks_per_cpu = chunks_in_file/no_of_processors;
        
        int lines_per_cpu = (total_linecount/no_of_processors); //used if each processor cannot get a minimum of one chunk to read

        //If each processor can get a minimum of one chunk
        if (chunks_per_cpu > no_of_processors)
        {
            lines_per_cpu = lines_per_chunk * chunks_per_cpu;
        }
        
        int line_low = 0;
        int line_up = lines_per_cpu;

        if(total_linecount != 0)
        {
            for(int i = 0; i < no_of_processors-1; i++)
            {
                range_low[i] = line_low;
                range_up[i] = line_up;

                line_low = line_up;
                line_up = line_low + lines_per_cpu;
            }
            range_low[no_of_processors-1] = line_low;
            range_up[no_of_processors-1]=total_linecount;
        }
        else
        {
            for(int i=0; i < no_of_processors; ++i)
            {
                range_low[i] = 0;
                range_up[i] = 0;
            }
        }
    }
}

//Copy of the read function from pisvm-train, shows how data is read from an svmlight format
//file and put into the svm_problem struct that is then used in the pisvm-train main function
//main function should free everything in svm_problem after use since it is global
void read_problem_svmlite(const char *filename, svm_problem *buff)
{
    int elements, i, j;
    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        fprintf(stderr, "can't open input file %s\n", filename);
        exit(1);
    }

    buff->l = 0;
    elements = 0;
    while (1)
    {
        int c = fgetc(fp);
        switch (c)
        {
        case '\n':
            ++buff->l;
            break;
        case ':':
            ++elements;
            break;
        case EOF:
            goto out;
        default:;
        }
    }
out:
    rewind(fp);
    buff->y = Malloc(double, buff->l);
    buff->x = Malloc(Xfloat *, buff->l);
    buff->nz_idx = Malloc(int *, buff->l);
    buff->x_len = Malloc(int, buff->l);
    //TODO: Check if not needed - loop sets prob.x_len[i] = 0 for i=0 to prob.l
    //memset(prob.x_len, 0, sizeof(int)*prob.l);
    Xfloat *x_space = Malloc(Xfloat, elements);
    int *nz_idx_space = Malloc(int, elements);

    buff->max_idx = 0;
    j = 0;
    for (i = 0; i < buff->l; i++)
    {
        double label;
        buff->x[i] = &x_space[j];
        buff->nz_idx[i] = &nz_idx_space[j];

        buff->x_len[i] = 0;
        int ignore = fscanf(fp, "%lf", &label); //read double
        buff->y[i] = label;
        while (1)
        {
            int c;
            do
            {
                c = getc(fp);
                if (c == '\n')
                    goto out2;
            } while (isspace(c));

            ungetc(c, fp);
            //	  fscanf(fp,"%d:%lf",&nz_idx_space[j],&x_space[j]);

            //decimal integer:floating point number, the integer has to be 1-based and is put in nz_idx_space (index space), the float is put in x_space
            ignore = fscanf(fp, "%d:%f", &nz_idx_space[j], &x_space[j]);

            if (nz_idx_space[j] == 0)
            {
                fprintf(stderr, "ERROR: Feature indices need to be 1-based!\n");
                exit(1);
            }
            --nz_idx_space[j]; // we need zero based indices
            ++buff->x_len[i];
            ++j;
        }
    out2:
        if (j >= 1 && nz_idx_space[j - 1] + 1 > buff->max_idx)
        {
            buff->max_idx = nz_idx_space[j - 1] + 1;
        }
    }

    float feature_density = 100.0 * elements / (buff->l * buff->max_idx);
    if (feature_density > 50)
    {
        printf("The features from the model file have a density of %.2f%%. \n"
               "You %s consider using the -D flag to use a dense feature representation.\n",
               feature_density, feature_density > 75 ? (feature_density > 90 ? "SHOULD" : "should") : "might");
    }

    fclose(fp);
}

//Copy of the read function from pisvm-train that reads data in a dense format from an svmlight
//file, and put into the svm_problem struct that is then used in the pisvm-train main function
//modified to place output into buff instead of global
void read_problem_svmlite_dense(const char *filename, svm_problem *buff)
{
    int i, j;
    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        fprintf(stderr, "can't open input file %s\n", filename);
        exit(1);
    }

    buff->l = 0;
    buff->max_idx = 0;
    while (1)
    {
        if (fscanf(fp, "%*f") == EOF)
            break;
        while (1)
        {
            int c;
            do
            {
                c = getc(fp);
            } while (isspace(c) && c != '\n');
            if (c == '\n')
            {
                break;
            }
            else if (c == EOF)
            {
                goto out;
            }
            else
            {
                ungetc(c, fp);
                int idx;
                int ret = fscanf(fp, "%d:%*f", &idx);
                if (ret == EOF)
                {
                    goto out;
                }
                if (idx > buff->max_idx)
                    buff->max_idx = idx;
            }
        }
        buff->l++;
    }
out:
    rewind(fp);
    buff->y = Malloc(double, buff->l);
    buff->x = Malloc(Xfloat *, buff->l);
    buff->nz_idx = Malloc(int *, buff->l);
    buff->x_len = Malloc(int, buff->l);

    Xfloat *x_space = Malloc(Xfloat, buff->l * buff->max_idx);
    int *nz_idx_space = Malloc(int, buff->max_idx);

    for (i = 0; i < buff->max_idx; i++)
    {
        nz_idx_space[i] = i;
    }

    //prob.max_idx = 0;
    //j=0;
    for (i = 0; i < buff->l; i++)
    {
        double label;
        buff->x[i] = &x_space[buff->max_idx * i];
        buff->nz_idx[i] = nz_idx_space;
        buff->x_len[i] = buff->max_idx;
        int ignore = fscanf(fp, "%lf", &label);
        buff->y[i] = label;
        j = 0;
        while (1)
        {
            int c, idx;
            Xfloat value;
            do
            {
                c = getc(fp);
            } while (isspace(c) && c != '\n');

            if (c == '\n')
                break;

            ungetc(c, fp);
            //	  fscanf(fp,"%d:%lf",&nz_idx_space[j],&x_space[j]);
            ignore = fscanf(fp, "%d:%f", &idx, &value);

            if (idx == 0)
            {
                fprintf(stderr, "ERROR: Feature indices need to be 1-based!\n");
                exit(1);
            }
            idx -= 1; // we need zero based indices
            for (; j < idx; j++)
            {
                buff->x[i][j] = 0;
            }
            j = idx + 1;
            buff->x[i][idx] = value;
        }
    }

    fclose(fp);
}

//This function splits prob into segments
//each rank gets a portion of the prob struct close to to n/numsegments
//where n is the number of lines in the input file
void split_svm_problem(svm_problem *inprob, svm_problem *buff, int my_segment, int num_segments, const char *input_filename, int denseformat)
{
    //we need to know how many lines each segment has
    int prob_length = inprob->l;
    int seg_len = static_cast<int>(floor(prob_length / num_segments));
    int final_seg_len = (prob_length % seg_len) + seg_len;
    int my_seg_len = 0;
    if (final_seg_len == 0)
    {
        final_seg_len = seg_len;
    }
    if (my_segment == num_segments - 1)
    {
        my_seg_len = final_seg_len;
    }
    else
    {
        my_seg_len = seg_len;
    }
    int my_start_point = seg_len * my_segment;
    int my_end_line = my_start_point + my_seg_len;
    //allocate stuff
    svm_problem temp_prob;
    temp_prob.y = Malloc(double, my_seg_len);
    temp_prob.x = Malloc(Xfloat *, my_seg_len);
    temp_prob.nz_idx = Malloc(int *, my_seg_len);
    temp_prob.x_len = Malloc(int, my_seg_len);
    temp_prob.l = my_seg_len;
    int tmp_max_idx = 0;

    //Iterate though all values in prob, and assign values in temp_prob
    for (int i = 0; i != my_seg_len; i++)
    {
        temp_prob.y[i] = inprob->y[my_start_point + i];
        temp_prob.x_len[i] = inprob->x_len[my_start_point + i];
        temp_prob.x[i] = Malloc(Xfloat, inprob->x_len[i]);
        temp_prob.nz_idx[i] = Malloc(int, inprob->x_len[i]);
        for (int n = 0; n != inprob->x_len[my_start_point + i]; n++)
        {
            temp_prob.x[i][n] = inprob->x[my_start_point + i][n];
            temp_prob.nz_idx[i][n] = inprob->nz_idx[my_start_point + i][n];
            //we need to check these because the problem section may not have the same max_idx as the full problem.
            if (temp_prob.nz_idx[i][n] > tmp_max_idx)
            {
                tmp_max_idx = temp_prob.nz_idx[i][n];
            }
        }
    }
    temp_prob.max_idx = tmp_max_idx + 1;
    deep_copy_svm_problem(&temp_prob, buff);
}

//This function reads data from an hdf5 in paralell, placing
//the segstart to segstart+(seglength-1) (inclusive, 0-indexed)
//vectors from the input file into prob
//This code is based on, with permission, read_problem_hdf5
// by Sigurður Páll Behrend, 2018
void read_problem_hdf5_parallel(svm_problem *buff, int rank, int total_ranks, const char *input_filename, bool denseformat)
{
    // int *l_up = (int *) malloc(total_ranks*sizeof(int));
    // int *l_low = (int *) malloc(total_ranks*sizeof(int));
    int *l_up = (int *)malloc(total_ranks * sizeof(int));
    int *l_low = (int *)malloc(total_ranks * sizeof(int));
    int l_low_loc, l_up_loc;
    int local_l = 0;
    //clock_t begin = clock();
    try
    {
        H5::Exception::dontPrint();
        H5::H5File file(input_filename, H5F_ACC_RDONLY); //read access only

        //we open the three datasets from the file
        H5::DataSet dsclass = file.openDataSet(CLASSSET_NAME);
        H5::DataSet dsfeatureset = file.openDataSet(FEATURESET_NAME);
        H5::DataSet dsdensitymap = file.openDataSet(DENSITYMAP_NAME);

        //The chunk count is written in the file metadata
        int chunkcount = 0;
        H5::Attribute attr_chunkcount = file.openAttribute("ChunkCount");
        H5::DataType typecc = attr_chunkcount.getDataType(); //Extract the datatype
        attr_chunkcount.read(typecc, &chunkcount);           //Read the feature count value

        //we need to read the feature count, the value depends on if we want dense data or not
        //we use the value to allocate space for the data we intend to read
        int featurecount = 0;
        H5::Attribute attr_featurecount = file.openAttribute("SparseFeatureCount");
        //H5::Attribute attr_featurecount = denseformat == true ? file.openAttribute("DenseFeatureCount") : file.openAttribute("SparseFeatureCount");

        H5::DataType type = attr_featurecount.getDataType(); //Extract the datatype
        attr_featurecount.read(type, &featurecount);         //Read the value

        //we will need two dataspaces, since the dataspace for dsfeatureset and dsdensitymap is the same
        H5::DataSpace filespace1d = dsclass.getSpace();
        H5::DataSpace filespace2d = dsfeatureset.getSpace();

        //extract the amount of records(lines) and features(columns) in dsfeatureset
        hsize_t dims_out[2];
        int ndims = filespace2d.getSimpleExtentDims(dims_out, NULL);
        int line_count = (int)dims_out[0];
        int max_feature_id = (int)dims_out[1];

        setup_range_hdf5(l_low, l_up, line_count, total_ranks, chunkcount);
        l_low_loc = l_low[rank];        //local lower line count (specific for this processors)
        l_up_loc = l_up[rank];          //local upper line count (specific for this processor)
        local_l = l_up_loc - l_low_loc; //local total line count
        //printf("I am rank %d, gonna read in vectors %d to %d\n", rank, l_low_loc, l_up_loc);

        //how we can use the local lower and local upper to set the count and offset for this processors particular read section
        hsize_t offset[2] = {l_low_loc, 0};           //{ start_at_line, start_at_column }, start_at_line is relevanto the processor rank
        hsize_t count[2] = {local_l, max_feature_id}; //{ lines_read, columns read }, lines read is relevant to the processor rank.
        hsize_t count2[2] = {local_l, 1};             //this is for reading the class_dataset since it is 1d
        //These arrays must be allocated on the heap because of the splits
        //they also have to be that big because that is how we read them from the file
        //we must then loop through then and put the data into nz_idx_space and x_space
        double *class_dataset = Malloc(double, local_l);                  //we initialize an array to hold the class value
        float *feature_dataset = Malloc(float, local_l *max_feature_id);  //an array to hold all values
        bool *densitymap_dataset = Malloc(bool, local_l *max_feature_id); //We initialize an array to hold bool values that tell us if the value was found in the file or if it is a filler to create dense data

        //we set up the hyperslab
        filespace1d.selectHyperslab(H5S_SELECT_SET, count2, offset);
        filespace2d.selectHyperslab(H5S_SELECT_SET, count, offset);
        //we need two memory dataspaces, one for dim1 and one for dim2
        hsize_t col_dims1[1];
        col_dims1[0] = local_l;
        H5::DataSpace mspace_1d(1, col_dims1);
        hsize_t col_dims2[2];
        col_dims2[0] = local_l;
        col_dims2[1] = max_feature_id;
        H5::DataSpace mspace_2d(2, col_dims2);
        //we now read the dataset
        dsclass.read(class_dataset, H5::PredType::NATIVE_DOUBLE, mspace_1d, filespace1d);
        dsfeatureset.read(feature_dataset, H5::PredType::NATIVE_FLOAT, mspace_2d, filespace2d);
        dsdensitymap.read(densitymap_dataset, H5::PredType::NATIVE_UCHAR, mspace_2d, filespace2d);

        //start initializing values
        //we must find out the correct max_idx for each of the splits

        buff->l = local_l;
        buff->y = Malloc(double, local_l); //the label/class
        buff->x = Malloc(Xfloat *, local_l);
        buff->nz_idx = Malloc(int *, local_l);
        buff->x_len = Malloc(int, local_l);
        x_space = Malloc(Xfloat, local_l * max_feature_id);
        nz_idx_space = Malloc(int, local_l *max_feature_id);

        int j = 0;      //j will keep track of where we add items into x_space and nz_idx_space
        int max_id = 0; //we need to keep track of the highest feature id

        for (int i = 0; i < local_l; i++)
        {
            //We store the label
            buff->y[i] = class_dataset[i];

            buff->nz_idx[i] = &nz_idx_space[j]; //and we store a pointer to the first featureid
            buff->x[i] = &x_space[j];           //we store a pointer to the first value

            buff->x_len[i] = 0;
            for (int e = 0; e < max_feature_id; e++)
            {
                int isindataset = densitymap_dataset[i * max_feature_id + e];
                if ((bool)isindataset == true || denseformat == true) //this value was in the dataset
                {
                    nz_idx_space[j] = e; //e is the feature_ID, we want it zero-based just like it is stored in HDF5
                    //set max feature id value
                    if (e > max_id)
                    {
                        max_id = e;
                    }

                    x_space[j] = feature_dataset[i * max_feature_id + e];
                    j++; //we found a feature:value, increment j
                    buff->x_len[i]++;
                }
            }
        }
        //set mac_idx
        buff->max_idx = max_id + 1;
    }
    // catch failure caused by the H5File operations
    catch (H5::FileIException error)
    {
        printf("FileIException error\n");
        error.printError();
        exit(1);
    }
    // catch failure caused by the DataSet operations
    catch (H5::DataSetIException error)
    {
        printf("DataSetIException error\n");
        error.printError();
        exit(1);
    }
    // catch failure caused by the DataSpace operations
    catch (H5::DataSpaceIException error)
    {
        printf("DataSpaceIException error\n");
        error.printError();
        exit(1);
    }
    // catch failure caused by the DataSpace operations
    catch (H5::DataTypeIException error)
    {
        printf("DataTypeIException error\n");
        error.printError();
        exit(1);
    }

    //clock_t end = clock();
    //double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    //printf("Time spent: %f seconds\n", time_spent);
}

//returns the color of the mpi communicator rank should have for a given current_round
//we select one that has not previously been used to sidestep the issue of
//modifying an in-use communicator color
int get_communicator_color(int rank, int current_round, int size)
{
    int key = static_cast<int>(pow(2, current_round));
    return static_cast<int>(floor(rank / key)) + size * current_round;
}


//output timings to file
void write_times(std::string filename, std::chrono::high_resolution_clock::time_point io_start, std::chrono::high_resolution_clock::time_point io_end,std::chrono::high_resolution_clock::time_point cascade_start,std::chrono::high_resolution_clock::time_point cascade_end)
{
    std::ofstream myfile;
    myfile.open(filename + ".times");
    auto iotime = std::chrono::duration_cast<std::chrono::milliseconds>( io_end - io_start ).count();
    myfile << "IO + single core cascade time was: ";
    myfile << iotime;
    myfile << " milliseconds\n";

    auto cascadetime = std::chrono::duration_cast<std::chrono::milliseconds>( cascade_end - cascade_start ).count();
    myfile << "MPI cascade time was: ";
    myfile << cascadetime;
    myfile << " milliseconds\n";

    auto totaltime = std::chrono::duration_cast<std::chrono::milliseconds>(cascade_end - io_start ).count();
    myfile << "total time was: ";
    myfile << totaltime;
    myfile << " milliseconds\n";
    myfile.close();
}

void exit_with_help()
{
    int rank = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0)
    {
        fprintf(stdout,
            "Usage: csvm-train [options] input_file [output_file] \n"
            "If omitted the output filename will be the same as the source file but with an .h5 ending instead of .el ending.\n"
            "options:\n"
            "-s svm_type : set type of SVM (default 0)\n"
            "   note: 1-4 are not guarenteed to work\n"
            "	0 -- C-SVC\n"
            "	1 -- nu-SVC\n"
            "	2 -- one-class SVM\n"
            "	3 -- epsilon-SVR\n"
            "	4 -- nu-SVR\n"
            "-t kernel_type : set type of kernel function (default 2)\n"
            "	0 -- linear: u'*v\n"
            "	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
            "	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"
            "	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
            "-d degree : set degree in kernel function (default 3)\n"
            "-g gamma : set gamma in kernel function (default 1/k)\n"
            "-r coef0 : set coef0 in kernel function (default 0)\n"
            "-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"
            "-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
            "-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
            "-m cachesize : set cache memory size in MB (default 40)\n"
            "-e epsilon : set tolerance of termination criterion (default 0.001)\n"
            "-h shrinking: whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
            "-b probability_estimates: whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
            "-w weight: set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
            "-v n: n-fold cross validation mode, not implemented\n"
            "-o n: max. size of working set\n"
            "-q n: max. number of new variables entering working set\n"
            "-P n: number of passes the cascade will perform(work in progress)\n"
            "-if, -iformat format : set the format type of the input file\n"
            "   svmlight -- File is in svmlight format\n"
            "   hdf5 -- File is in HDF5 format as formatted by pisvm-parse, can be combined with -D to read HDF5 data in dense format\n"
            "-of, -oformat format : set the format type of the output file\n"
            "   svmlight -- File is in svmlight format\n"
            "   hdf5 -- File is in HDF5 format\n"
            "flags:\n"
            "-D: Assume the feature vectors are dense (default: sparse)\n"
            "-S n: cascade segment count per thread, must be a power of 2(default: 1)\n");
    }
    exit(1);
}

void parse_command_line(int argc, char **argv, char *input_filename, char *output_filename, int *passes, svm_parameter *param)
{
    //set safe default values
    param->svm_type = C_SVC;
    param->kernel_type = RBF;
    param->degree = 3;
    param->gamma = 0; // 1/k
    param->coef0 = 0;
    param->nu = 0.5;
    param->cache_size = 40;
    param->C = 1;
    param->eps = 1e-3;
    param->p = 0.1;
    param->shrinking = 1;
    param->probability = 0;
    param->nr_weight = 0;
    param->weight_label = NULL;
    param->weight = NULL;
    param->o = 2; // safe defaults
    param->q = 2;
    cross_validation = 0;
    dense_features = 0;

    input_file_type = svmlite;
    output_file_type = svmlite;
    seg_count = 1;

    // parse options
    int i;
    for (i = 1; i < argc; i++)
    {
        if (argv[i][0] != '-')
        {
            break; //all options must be prefixed with dash(-), no dash means no more options so we break and parse file names
        }
        //can't use switch because of it only works with single characters
        if (strcmp(argv[i], "-o") == 0) //chunk count is being set
        {
            i++;                      //advance one position
            param->o = atoi(argv[i]); //and then read the value
        }
        else if (strcmp(argv[i], "-q") == 0)
        {
            i++;
            param->q = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-s") == 0)
        {
            i++;
            param->svm_type = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-t") == 0)
        {
            i++;
            param->kernel_type = atoi(argv[i]);
            if (param->kernel_type < 0)
            {
                fprintf(stderr, "Invalid kernel type\n");
                exit_with_help();
            }
        }
        else if (strcmp(argv[i], "-d") == 0)
        {
            i++;
            param->degree = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-g") == 0)
        {
            i++;
            param->gamma = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-r") == 0)
        {
            i++;
            param->coef0 = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-n") == 0)
        {
            i++;
            param->nu = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-m") == 0)
        {
            i++;
            param->cache_size = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-c") == 0)
        {
            i++;
            param->C = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-e") == 0)
        {
            i++;
            param->eps = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-p") == 0)
        {
            i++;
            param->p = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-h") == 0)
        {
            i++;
            param->shrinking = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            i++;
            param->probability = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-v") == 0)
        {
            i++;
            cross_validation = 1;
            nr_fold = atoi(argv[i]);
            printf("Cross validation is not yet implemented in this version of csvm\n");
            exit(1);
        }
        else if (strcmp(argv[i], "-w") == 0)
        {
            i++;
            ++param->nr_weight;
            param->weight_label = (int *)realloc(param->weight_label, sizeof(int) * param->nr_weight);
            param->weight = (double *)realloc(param->weight, sizeof(double) * param->nr_weight);
            param->weight_label[param->nr_weight - 1] = atoi(&argv[i - 1][2]);
            param->weight[param->nr_weight - 1] = atof(argv[i]);
        }
        else if (strcmp(argv[i], "-D") == 0)
        {
            dense_features = 1;
        }
        else if (strcmp(argv[i], "-P") == 0)
        {
            i++;
            *passes = atoi(argv[i]);
            if (*passes < 1)
            {
                fprintf(stderr, "Error: number of passes must be a positive integer\n");
                exit(1);
            }
        }
        else if (strcmp(argv[i], "-S") == 0)
        {
            i++;
            seg_count = atoi(argv[i]);
            //check if segment count is correct
            if((seg_count & (seg_count - 1)) != 0 || seg_count < 0)
            {
                fprintf(stderr, "Error: segment count must be power of 2\n");
                exit(1);
            }
        }
        else if (strcmp(argv[i], "-iformat") == 0 || strcmp(argv[i], "-if") == 0)
        {
            i++;
            if (strcmp(argv[i], "hdf5") == 0)
            {
                input_file_type = hdf5;
            }
            else if (strcmp(argv[i], "svmlight") == 0)
            {
                input_file_type = svmlite;
            }
            else
            {
                fprintf(stderr, "Error: unknown file format specified for input, valid options are hdf5(default) and svmlight\n");
                exit(1);
            }
        }
        else if (strcmp(argv[i], "-oformat") == 0 || strcmp(argv[i], "-of") == 0)
        {
            i++;
            if (strcmp(argv[i], "hdf5") == 0)
            {
                output_file_type = hdf5;
            }
            else if (strcmp(argv[i], "svmlight") == 0)
            {
                output_file_type = svmlite;
            }
            else
            {
                fprintf(stderr, "Error: unknown file format specified for output, valid options are hdf5(default) and svmlight\n");
                exit(1);
            }
        }
        else //no valid option
        {
            fprintf(stderr, "unknown option\n");
            exit_with_help();
        }
    }

    //for loop has now parsed all the options, next up we determine the file names
    if (i >= argc) //if there are no arguments after the options (we need atleast the input file name
    {
        exit_with_help();
    }
    //if we get this far then the file name is next
    strcpy(input_filename, argv[i]);
    //Check if output filename was provided
    if (i == argc - 2) //There is exactly 1 additional parameter after the input file name
    {
        strcpy(output_filename, argv[i + 1]);
    }
    else if (i == argc - 1) //input file name is the last parameter, have to construct the output
    {
        char *p = strrchr(argv[i], '/'); //there might be an absolute path provided, strip that out so that output file will go to users home directory
        if (p == NULL)
            p = argv[i];
        else
            p++;

        //just the filename should be left now in *p, find the last dot and remove the .el ending if it is there
        char *lastdot = strrchr(p, '.');
        if (lastdot != NULL)
        {
            *lastdot = '\0';
        }
        if(output_file_type == hdf5)
        {
            sprintf(output_filename, "%s-out.h5", p); //append a file type ending
        }
        else
        {
            sprintf(output_filename, "%s.model", p); //append a file type ending
        }
    }
    else
    { //There are more parameters
        fprintf(stderr, "Error: There are unparsed parameters.\n");
        exit_with_help();
    }
}

const char *svm_type_table_l[] =
    {
        "c_svc", "nu_svc", "one_class", "epsilon_svr", "nu_svr", NULL};

const char *kernel_type_table_l[] =
    {
        "linear", "polynomial", "rbf", "sigmoid", NULL};

//Sample function that was migrated to pisvm-train, saves the model file in hdf5 format from
//a provided svm_model struct with all the parameters and SVs
int svm_save_model_hdf5(const char *model_file_name, const svm_model *model)
{
    //Fetch the parameters from the model
    const svm_parameter &param = model->param;

    //Don't save dense kernel type to file
    int kernel_type = param.kernel_type;
    int is_dense = kernel_type < 0;
    if (is_dense)
        kernel_type = -kernel_type - 1;

    try
    {
        //Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
        H5::Exception::dontPrint();

        //create the file
        H5::H5File file(model_file_name, H5F_ACC_TRUNC);

        //create a new dataspace for attributes
        H5::DataSpace attr_dataspace(H5S_SCALAR);

        //define datatypes
        H5::IntType int_type(H5::PredType::NATIVE_INT);        //define a type for INTs
        H5::FloatType float_type(H5::PredType::NATIVE_FLOAT);  //define a type for Floats
        H5::PredType double_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for class_dataset, H5T_NATIVE_DOUBLE
        H5::StrType str_type(H5::PredType::C_S1, 20);          //define a type for strings, TODO, when size is set to H5T_VARIABLE it causes segmentation fault, figure out why, until then 20 should be enough for everyone

        //Store svm_type, %s
        H5::Attribute attr_svm_type = file.createAttribute("svm_type", str_type, attr_dataspace);
        attr_svm_type.write(str_type, svm_type_table_l[param.svm_type]);
        attr_svm_type.close();

        //Store kernel_type, %s
        H5::Attribute attr_kernel_type = file.createAttribute("kernel_type", str_type, attr_dataspace);
        attr_kernel_type.write(str_type, kernel_type_table_l[param.kernel_type]);
        attr_kernel_type.close();

        if (kernel_type == POLY)
        {
            //Store degree, %d
            H5::Attribute attr_degree = file.createAttribute("degree", int_type, attr_dataspace);
            attr_degree.write(int_type, &param.degree);
            attr_degree.close();
        }

        if (kernel_type == POLY || kernel_type == RBF || kernel_type == SIGMOID)
        {
            //Store gamma, %g
            H5::Attribute attr_gamma = file.createAttribute("gamma", double_type, attr_dataspace);
            attr_gamma.write(double_type, &param.gamma);
            attr_gamma.close();
        }

        if (kernel_type == POLY || kernel_type == SIGMOID)
        {
            //Store coef0, %g
            H5::Attribute attr_coef0 = file.createAttribute("coef0", double_type, attr_dataspace);
            attr_coef0.write(double_type, &param.coef0);
            attr_coef0.close();
        }

        //Store nr_class, number of classes, %d
        int nr_class = model->nr_class;
        H5::Attribute attr_nr_class = file.createAttribute("nr_class", int_type, attr_dataspace);
        attr_nr_class.write(int_type, &nr_class);
        attr_nr_class.close();

        //Store total_sv, number of SVs (lines), %d
        int total_sv = model->l;
        H5::Attribute attr_total_sv = file.createAttribute("total_sv", int_type, attr_dataspace);
        attr_total_sv.write(int_type, &total_sv);
        attr_total_sv.close();

        //everything below is stored in arrays
        int value_count = nr_class * (nr_class - 1) / 2;

        //we create a chunk dimension for compression
        hsize_t chunk_dims[2];

        chunk_dims[0] = model->l;       //this is how many rows each chunk is
        chunk_dims[1] = model->max_idx; //this is how many columns the chunk is

        //create a property list, used for compression
        H5::DSetCreatPropList proplist;
        proplist.setChunk(2, chunk_dims); //2 because this is a 2D array
        proplist.setDeflate(9);           //0-9, lower = less compression, set highest compression

        //we need two differently sized dataspaces for 1D datasets
        hsize_t dim1d[1];                                //1D dataspace
        dim1d[0] = value_count;                          //number of rows, it only has one column
        H5::DataSpace dataspace_1d_valuecount(1, dim1d); //this is 1 because this will be stored as a 1D array

        dim1d[0] = nr_class;                          //number of rows, it only has one column
        H5::DataSpace dataspace_1d_nrclass(1, dim1d); //this is 1 because this will be stored as a 1D array

        //Store rho values, %g
        H5::DataSet rho_class = file.createDataSet("rho", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
        rho_class.write(model->rho, double_type);                                                //write the data to the dataset
        rho_class.close();                                                                       //close this dataset

        //Store label values, $d
        if (model->label)
        {
            H5::DataSet label_class = file.createDataSet("label", int_type, dataspace_1d_nrclass); //create the dataset, this isn't so big so no need to compress it
            label_class.write(model->label, int_type);                                             //write the data to the dataset
            label_class.close();                                                                   //close this dataset
        }

        //Store probA, %g
        if (model->probA) //regression has probA only
        {
            H5::DataSet probA_class = file.createDataSet("probA", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
            probA_class.write(model->probA, double_type);                                                //write the data to the dataset
            probA_class.close();                                                                         //close this dataset
        }

        //Store probB, %g
        if (model->probB)
        {
            H5::DataSet probB_class = file.createDataSet("probB", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
            probB_class.write(model->probB, double_type);                                                //write the data to the dataset
            probB_class.close();                                                                         //close this dataset
        }

        //Store number of SVs, %d
        if (model->nSV)
        {
            H5::DataSet nSV_class = file.createDataSet("nr_sv", int_type, dataspace_1d_nrclass); //create the dataset, this isn't so big so no need to compress it
            nSV_class.write(model->nSV, int_type);                                               //write the data to the dataset
            nSV_class.close();                                                                   //close this dataset
        }

        //Store the SV coefficients, create a dataspace for them
        hsize_t dim2d_coef[2];                          //2D dataspace
        dim2d_coef[0] = total_sv;                       //number of rows
        dim2d_coef[1] = nr_class - 1;                   //number of columns
        H5::DataSpace dataspace_2d_coef(2, dim2d_coef); //2 because we want to save a 1D array as 2D

        //Convert the coefficients into a linear array
        double *coefficient_dataset = Calloc(double, total_sv *(nr_class - 1));

        for (int i = 0; i < total_sv; i++)
        {
            for (int j = 0; j < nr_class - 1; j++)
            {
                coefficient_dataset[i * (nr_class - 1) + j] = model->sv_coef[j][i];
            }
        }

        //Create and process the coefficient dataset
        H5::PredType coef_type(H5::PredType::NATIVE_DOUBLE);                               //define the datatype for class_dataset, H5T_NATIVE_DOUBLE
        H5::DataSet dsclass = file.createDataSet("sv_coef", coef_type, dataspace_2d_coef); //create the dataset, this isn't so big so no need to compress it
        dsclass.write(coefficient_dataset, coef_type);                                     //write the data to the dataset
        dsclass.close();                                                                   //close this dataset

        free(coefficient_dataset); //

        //Store the SVs themselves along with a binary map to indicate if a feature:value set is part of the SV
        hsize_t dim2d_sv[2];                        //2D dataspace
        dim2d_sv[0] = total_sv;                     //number of rows
        dim2d_sv[1] = model->max_idx;               //number of columns
        H5::DataSpace dataspace_2d_sv(2, dim2d_sv); //2 because we want to save a 1D array as 2D

        double *sv_dataset = Calloc(double, total_sv * model->max_idx);     //An array to hold the feature values
        bool *densitymap_dataset = Calloc(bool, total_sv * model->max_idx); //An array to hold bool values that tell us if that value was in the file or not, used to create sparse data

        int featurecount = 0;
        for (int i = 0; i < total_sv; i++)
        {
            for (int j = 0; j < model->sv_len[i]; j++)
            {
                sv_dataset[i * model->max_idx + model->nz_sv[i][j]] = model->SV[i][j];
                densitymap_dataset[i * model->max_idx + model->nz_sv[i][j]] = true;
                featurecount++;
            }
        }

        //Save FeatureCount as an attribute, %d
        H5::Attribute attr_featurecount = file.createAttribute("FeatureCount", int_type, attr_dataspace);
        attr_featurecount.write(int_type, &featurecount);
        attr_featurecount.close();

        //process the sv_dataset dataset
        H5::PredType sv_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for sv_dataset, H5T_NATIVE_FLOAT
        H5::DataSet dsSVset = file.createDataSet("SV", sv_type, dataspace_2d_sv, proplist);
        dsSVset.write(sv_dataset, sv_type); //write the data to the dataset
        dsSVset.close();                    //close this dataset

        //process the densitymap dataset
        H5::PredType densitymap_type(H5::PredType::NATIVE_UCHAR); //define the datatype for densitymap_dataset, the data is bool but we store it as unsigned char for reduced space
        H5::DataSet dsdensitymap = file.createDataSet(DENSITYMAP_NAME, densitymap_type, dataspace_2d_sv, proplist);
        dsdensitymap.write(densitymap_dataset, densitymap_type); //write the data to the dataset
        dsdensitymap.close();                                    //close this dataset

        file.close(); //finally close the file

        return 0;
    }
    //Generic HDF5 exception block since we just print the error
    catch (H5::Exception ex)
    {
        ex.printError(stderr);
    }
    return -1;
}

//This performs single-thread cascading, it uses 1 core to perform a cascade
//and should(tm) place the output of that into the svm_problm *buff, which is then usable
//by the parallel cascade
void segment_cascade(svm_problem *buff, int mpi_rank, int segcount, const char *input_filename, bool denseformat, svm_parameter *param, bool read_hdf5)
{
    int mpi_size, virt_size;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm my_comm;
    MPI_Comm_split(MPI_COMM_WORLD, mpi_rank, mpi_rank, &my_comm);
    //simplest way is to assign 2 vectors and rotate them every loop iteration
    std::vector<svm_problem> probsA(seg_count);
    std::vector<svm_problem> probsB(seg_count / 2);
    virt_size = mpi_size * seg_count;
    //keep track of which probs vector we are reading form
    bool read_A = true;
    int working_count = seg_count;
    //initialize by reading into vector from file

    if(read_hdf5)
    {
        for(int i = 0; i != working_count; i++)
        {
            int virt_rank = (mpi_rank * segcount) + i;
            read_problem_hdf5_parallel(&probsA[i], virt_rank, virt_size, input_filename, denseformat);
        }
    }
    else
    {
        svm_problem temp_prob;
        //first we need to read the file into prob
        if (denseformat == 1)
        {
            read_problem_svmlite_dense(input_filename, &temp_prob);
        }
        else
        {
            read_problem_svmlite(input_filename, &temp_prob);
        }
        for(int i = 0; i != working_count; i++)
        {
            int virt_rank = (mpi_rank * segcount) + i;
            split_svm_problem(&temp_prob, &probsA[i], virt_rank, virt_size, input_filename, denseformat);
        }
    }
    while(working_count > 1)
    {
        for(int n = 0; n < working_count/2; n++)
        {
            svm_model modelA; 
            svm_model modelB;
            if(read_A)
            {
                modelA = *svm_train(&probsA[n*2], param, my_comm);
                modelB = *svm_train(&probsA[(n*2)+1], param, my_comm);
            }
            else
            {
                modelA = *svm_train(&probsB[n*2], param, my_comm);
                modelB = *svm_train(&probsB[(n*2)+1], param, my_comm);
            }
            svm_problem probA;
            svm_problem probB;
            convert_svm_model_to_problem(&modelA, &probA);
            convert_svm_model_to_problem(&modelB, &probB);
            //if we read from probsA we write to probsB and vice versa
            if(read_A)
            {
                concat_svm_problems(&probA, &probB, &probsB[n]);
            }
            else
            {
                concat_svm_problems(&probA, &probB, &probsA[n]);
            }
        }
        working_count /= 2;
        //if we just finished reading from A, clear it and reinitialize, same for B
        if(read_A)
        {
            probsA.clear();
            probsA.resize(working_count/2);
        }
        else
        {
            probsB.clear();
            probsB.resize(working_count/2);
        }
        read_A = !read_A;
    }
    //if the last vector we read from was probsB, we inverted read_A to
    //true at the end of the loop, and need to read A
    if(read_A)
    {
        deep_copy_svm_problem(&probsA[0],buff);
    }
    else
    {
        deep_copy_svm_problem(&probsB[0],buff);
    }
}

int main(int argc, char **argv)
{

    int size, rank;
    int old_rank, new_rank, old_size = -1;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //check if core count is correct
    if((size & (size - 1)) != 0)
    {
        fprintf(stderr, "Error: thread count must be power of 2\n");
        exit(1);
    }

    std::chrono::high_resolution_clock::time_point io_start;
    std::chrono::high_resolution_clock::time_point io_end;
    std::chrono::high_resolution_clock::time_point cascade_start;
    std::chrono::high_resolution_clock::time_point cascade_end;

    int current_round = 0;
    int passes = 1;
    int comm_color;

    struct svm_parameter param;

    char input_file_name[1024];
    char output_file_name[1024];

    parse_command_line(argc, argv, input_file_name, output_file_name, &passes, &param);

    int total_rounds = static_cast<int>(log2(size*seg_count));
    int parallel_rounds = static_cast<int>(log2(size));

    if (rank == 0)
    {
        printf("Working with %d ranks, passes: %d , cascade layers: %d , of which parallel: %d , segments per thread: %d\n\n", size, passes, total_rounds, parallel_rounds, seg_count);
    }

    //first we perform the "multiple segments per core" logic, before moving on to the parallel cascade

    //start the IO timer at the same time
    io_start = std::chrono::high_resolution_clock::now();
    //if we have a segcount of 1 we can use the simple file read first
    if (seg_count == 1)
    {
        if (input_file_type == hdf5)
        {
            read_problem_hdf5_parallel(&prob, rank, size, input_file_name, dense_features);
        }
        else
        {
            svm_problem temp_prob;
            //first we need to read the file into prob
            if (dense_features == 1)
            {
                read_problem_svmlite_dense(input_file_name, &temp_prob);
            }
            else
            {
                read_problem_svmlite(input_file_name, &temp_prob);
            }
            //then split it
            split_svm_problem(&temp_prob, &prob, rank, size, input_file_name, dense_features);
        }
    }
    //if we have a higher segcount, we need to perform the segment_cascade function first
    else
    {
        if(input_file_type == hdf5)
        {
            segment_cascade(&prob, rank, seg_count, input_file_name, dense_features, &param, true);
        }
        else
        {
            segment_cascade(&prob, rank, seg_count, input_file_name, dense_features, &param, false);
        }
    }
    //stop the IO timer at the same time
    io_end = std::chrono::high_resolution_clock::now();


    const char *error_msg;
    error_msg = svm_check_parameter(&prob, &param);
    if (error_msg)
    {
        fprintf(stderr, "Error: %s\n", error_msg);
        exit(1);
    }

    const std::string ifname = "dump/" + std::to_string(current_round) + "/probrd";

    //start the cascade timer at the same time
    cascade_start = std::chrono::high_resolution_clock::now();

    for (int pass = 0; pass != passes; pass++)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        comm_color = rank;

        //if this is not the first pass we want prob to be a concatination of
        //its initial prob and the previous passes surviving support vectors
        //else we want it to simply be the file contents
        if (pass != 0)
        {
            destroy_svm_problem_safe(&prob_B);
            destroy_svm_problem_safe(&prob_A);
            destroy_svm_problem_safe(&prob);
            convert_svm_model_to_problem(model, &prob_B);
            bcast_svm_problem(0, rank, &prob_B, MPI_COMM_WORLD);
            svm_destroy_model(model);
            concat_svm_problems(&initial_prob, &prob_B, &prob);
        }

        MPI_Comm mycomm;
        MPI_Comm_split(MPI_COMM_WORLD, comm_color, rank, &mycomm);

        //initial model creation using data read form files

        model = svm_train(&prob, &param, mycomm);
        current_round++;
        while (current_round <= parallel_rounds)
        {
            MPI_Barrier(MPI_COMM_WORLD);
            //ensure the prob_A, prob_B and prob structs are clear
            destroy_svm_problem_safe(&prob_A);
            destroy_svm_problem_safe(&prob_B);
            destroy_svm_problem_safe(&prob);

            //set up new mpi communicator
            MPI_Comm_rank(mycomm, &old_rank);
            MPI_Comm_size(mycomm, &old_size);
            comm_color = get_communicator_color(rank, current_round, size);
            MPI_Comm_split(MPI_COMM_WORLD, comm_color, rank, &mycomm);
            MPI_Comm_rank(mycomm, &new_rank);

            if (old_rank == 0)
            {
                if (new_rank != 0)
                {
                    //we know this must be the sender
                    convert_svm_model_to_problem(model, &prob_B);
                    
                    send_svm_problem(0, &prob_B, mycomm);
                }
                else
                {
                    //we know this must be the receiver, and the sender will be the lowest rank in the current communicator
                    //that was not in the previous communicator
                    convert_svm_model_to_problem(model, &prob_A);
                    recv_svm_problem(old_size, &prob_B, mycomm);
                    concat_svm_problems(&prob_A, &prob_B, &prob);
                }
            }
            bcast_svm_problem(0, new_rank, &prob, mycomm);
            destroy_svm_model_safe(model);
            model = svm_train(&prob, &param, mycomm);
            current_round++;
        }
    }

    //all done, output file
    if (rank == 0)
    {
        if (output_file_type == svmlite)
        {
            svm_save_model(output_file_name, model);
        }
        else
        {
            svm_save_model_hdf5(output_file_name, model);
        }
    }

    //stop the cascade timers at the same time
    cascade_end = std::chrono::high_resolution_clock::now();

    if(rank == 0)
    {
        write_times(output_file_name,io_start, io_end, cascade_start, cascade_end);
    }

    MPI_Finalize();
    return 0;
}

CFLAGS = -fPIC -Wall -Og -fsanitize=leak -fsanitize=undefined

all:
	mpicxx -Wall -O3 -I. -Isrc/ -Irc/smo/ -D SOLVER_PSMO -c src/svm-comm.cpp
	h5c++ -std=c++11 -Wall -O3 -Isrc -Ipsmo svm-comm.o src/csvm-train.cpp -o build/csvm-train.o

clean:
	rm build/csvm-train.o
